//Castiel Le ID:1933080
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

public class RpsChoice implements EventHandler<ActionEvent> {
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String userchoice;
	private RpsGame rps;
	public RpsChoice(TextField message, TextField wins, TextField losses, TextField ties, String userchoice, RpsGame rps) {
		this.message = message;
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.userchoice = userchoice;
		this.rps = rps;
	}
	@Override 
	public void handle(ActionEvent e) {
		String msg = rps.playRound(userchoice);
		message.setText(msg);
		wins.setText("wins: " + rps.getWins());
		losses.setText("losses: " + rps.getLosses());
		ties.setText("ties: " + rps.getTies());
	}
}
