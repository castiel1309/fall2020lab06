//Castiel Le ID:1933080
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
	
public class RpsApplication extends Application {
	private RpsGame rps = new RpsGame();
	public void start(Stage stage) {
			Group root = new Group(); 
			
			HBox buttons = new HBox();
			Button rock = new Button("rock");
			Button scissors =  new Button("scissors");
			Button paper = new Button("paper");
			
			HBox textFields = new HBox();
			TextField message = new TextField("Welcome!");
			message.setPrefWidth(200);
			TextField wins = new TextField("wins: 0");
			wins.setPrefWidth(200);
			TextField losses = new TextField("losses: 0");
			losses.setPrefWidth(200);
			TextField ties = new TextField("ties: 0");
			ties.setPrefWidth(200);
			
			VBox overall = new VBox();
			
			buttons.getChildren().addAll(rock, scissors, paper);
			textFields.getChildren().addAll(message, wins, losses, ties);
			overall.getChildren().addAll(buttons,textFields);
			root.getChildren().add(overall);
			
			RpsChoice rockchoice = new RpsChoice(message, wins, losses, ties,"rock", rps);
			rock.setOnAction(rockchoice);
			RpsChoice scissorschoice = new RpsChoice(message, wins, losses, ties, "scissors", rps);
			scissors.setOnAction(scissorschoice);
			RpsChoice paperchoice = new RpsChoice(message, wins, losses, ties, "paper", rps);
			paper.setOnAction(paperchoice);
			//scene is associated with container, dimensions
			Scene scene = new Scene(root, 650, 300); 
			scene.setFill(Color.BLACK);

			//associate scene to stage and show
			stage.setTitle("Rock Paper Scissors"); 
			stage.setScene(scene); 
			
			stage.show(); 
		}
		
	    public static void main(String[] args) {
	        Application.launch(args);
	    }
	}    


