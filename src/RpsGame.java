//Castiel Le ID: 1933080
import java.util.*;

public class RpsGame {
	private int wins = 0;
	private int ties = 0;
	private int losses = 0;
	
	public int getWins() {
		return wins;
	}
	
	public int getTies() {
		return ties;
	}
	
	public int getLosses() {
		return losses;
	}
	
	public String playRound(String input) {
		Random randomchoice = new Random();
		int computerchoice = randomchoice.nextInt(2);
		if(input.equals("rock")) {
			if(computerchoice == 0) {
				ties++;
				return ("Computer played rock and it's a tie!");
			}
			else if(computerchoice == 1) {
				wins++;
				return ("Computer played scissors and you won!");
			}
			else {
				losses++;
				return ("Computer played paper and you lost!");
			}
		}
		else if(input.equals("scissors")) {
			if(computerchoice == 1) {
				ties++;
				return ("Computer played scissors and it's a tie!");
			}
			else if(computerchoice == 2) {
				wins++;
				return ("Computer played paper and you won!");
			}
			else {
				losses++;
				return ("Computer played rock and you lost!");
			}
		}
		else if(input.equals("paper")) {
			if(computerchoice == 2) {
				ties++;
				return ("Computer played paper and it's a tie!");
			}
			else if(computerchoice == 0) {
				wins++;
				return ("Computer played rock and you won!");
			}
			else {
				losses++;
				return ("Computer played scissors and you lost!");
			}
		}
		else {
			return ("Invalid value!");
		}
	}
}
	
